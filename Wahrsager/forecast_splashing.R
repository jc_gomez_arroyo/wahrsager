#'@author RitterDragon -> SquirrelBirds Team
#'@description Forecast Splashing Module
#'
#'After define de best output from a determined
#'Time series, this module perform the final step
#'for the Forecasting Process - The Hierarchical
#'Reconciliation.


#' @note To reconciliate within levels interactions with level and upper_splash params
splash_n_reconciliate <- function(dt_forecasted, model, 
                                  level, dest_path, upper_splash,
                                  level_props, post){
        tryCatch({
                if(post){
                        
                        dt_forecasted <- file_path
                        
                        lineaf <- dt_forecasted[, unique(linea)]
                        
                        tiendaf <- dt_forecasted[, unique(tienda)]
                        
                } else {
                        dt_forecasted[, ':='(fecha = as.Date(fecha),
                                             n = round(mean),
                                             model = NULL,
                                             tienda = NULL,
                                             linea = NULL,
                                             mean = NULL)]
                }
                if(level != "GENERAL")
                        dt_forecasted <- reconciliate(dt_forecasted, upper_splash, level)
                dt_forecasted[n < 0, n := 0]
                setorder(level_props, -level_down_prop)
                safe.asign_splash <- safely(asign_splash, otherwise = NULL)
                result <- map(1:nrow(dt_forecasted), 
                                     safe.asign_splash,  
                                     dtf = dt_forecasted,
                                     props = level_props[!is.na(level_down_prop)],
                                     level = level)
                dt_splashed <- result %>% map_df("result")
                errors <- result %>% map("error") %>% compact()
                if(level == "linea"){
                        dt_splashed <- cbind(dt_splashed,
                                             tienda = as.character(level_props[, unique(tienda)]),
                                             linea = as.character(level_props[, unique(linea)]))
                } else if(level == "tienda"){
                        dt_splashed <- cbind(dt_splashed,
                                             tienda = as.character(level_props[, unique(tienda)]))
                }
                
                return(dt_splashed)
        },
        error = function(e){
                log_msg("Error en splashing_n_reconciliate",flog.error)
                log_msg(e$message, flog.error)
                persist_error_case(data.table(tienda = level_props[, unique(tienda)],
                                              linea = level_props[, unique(linea)],
                                              sku = "GENERAL",
                                              msg  = "non-forecastable"),
                                   dest_path)
        })
}


asign_splash <- function(i, dtf, props, level){
        n2splash <- dtf[i, n]
        props[, n:= NULL]
        if(n2splash < 2){
                props[which.max(level_down_prop), n := n2splash] %>%
                        .[!which.max(level_down_prop), n := 0]
        }else{
                props[, n := round(level_down_prop*n2splash)]
                }
        if(n2splash > props[, sum(n)]){
                delta <- n2splash - props[, sum(n)]
                delta_level <- as.data.table(sample_n(
                        props, 
                        delta, 
                        replace = FALSE))[, 
                                          level_down_prop := level_down_prop/sum(level_down_prop)][
                                                  , n := round(level_down_prop*delta)]
                if(level == "linea")
                        props[sku %in% delta_level$sku, n := n + delta_level$n]
                else if(level == "tienda")
                        props[linea %in% delta_level$linea, n := n + delta_level$n] 
                else
                        props[tienda %in% delta_level$tienda, n := n + delta_level$n]
        }
        
        if(nrow(props) != 0){
                if(level == "linea"){
                        dt_expanded <- cbind(dtf[i, .(fecha)], 
                                             props[n != 0, .(sku, n)])
                        dt_splashed <- dt_expanded[, .(n, sku, fecha)]
                } else if(level == "tienda"){
                        dt_expanded <- cbind(dtf[i, .(fecha)], 
                                             props[n != 0, .(linea, n)])
                        dt_splashed <- dt_expanded[, .(n, linea, fecha)]
                } else{
                        dt_expanded <- cbind(dtf[i, .(fecha)],
                                             props[n != 0, .(tienda, n)])
                        dt_splashed <- dt_expanded[, .(n, tienda, fecha)]
                }
                dt_splashed[n < 0, n := 0]
                return(dt_splashed)
        }
}

reconciliate <- function(forecasted, upper_splash, level){
        forecasted <- forecasted[, ':='(Week = week(fecha),
                                        Year = year(fecha))]
        upper_splash <- upper_splash[, ':='(Week = week(fecha),
                                            Year = year(fecha))]
        result <- unique(merge(forecasted[, .(fecha, Week, Year, n)], 
                               upper_splash[, .(Week, Year, n)], 
                               by = c("Year", "Week"),
                               all.x = TRUE) %>% 
                                 .[ , .(Year, Week, fecha, n.x, n.y)]) %>%
                .[is.na(n.y), n.y := 0] %>% 
                .[n.y != 0,':='(mean_n = round(((as.double(n.x) * lower.level.weight) +
                                                        (as.double(n.y) * upper.level.weight)) /
                                                       (lower.level.weight + upper.level.weight)))] %>%
                .[n.y == 0, mean_n := n.x] %>%
                .[, ':='(n = mean_n,
                         mean_n = NULL,
                         n.x = NULL,
                         n.y = NULL)]
        
        result <- standarize_dates(result, NULL)
        setorder(result, fecha)
        return(result)
}


compute_proportions<-function(dt,level,low_level){
        setorder(dt, -fecha)
        # year_init <- if(dt[, week(max(fecha))] >= 52) 1 else 2
        years <- dt[, unique(Year)][1:(proportions.years)]
        dt <- dt[Year %in% years,]
        if(is.null(level))
                dt[, n_level := sum(n)]
        else
                dt[, n_level := sum(n), 
                   by = c(level)]
        dt[, n_level_down := sum(n), 
           by = c(level,low_level)]
        dt[, ':='(level_down_prop = 
                          mean(n_level_down)/mean(n_level)), 
           by = c(level,low_level)]
        level_props <- unique(dt[, c(level,low_level, 
                                     "level_down_prop"),
                                 with=FALSE])
        return(level_props)
}