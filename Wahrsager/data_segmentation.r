#'@author RitterDragon -> SquirrelBirds
#'@description All function about data segmentation
#'that could be needed before Model Selection
#'Module, will be here.


#'@description Perform a segmentation of chunks from a
#'data.table object to different files.
#'@param x Data.table object to segment.
#'@param dest_path Destination Directory Path
dt_segmentation_n_persist <- function(x, dest_path){
     split_vector <- x[, unique(tienda)]
     for(i in split_vector){
          tryCatch({
               log_msg(paste("Tienda: ", i), flog.info)
               df <- x[tienda == i, ]
               log_msg(paste("Chunk Extract Length: ", nrow(df)), flog.info)
               name <- paste(dest_path, "preprocessed", i, sep = "/")
               file_name <- paste(name, ".csv", sep = "")
               log_msg(file_name, flog.info)
               fwrite(df, 
                      file_name,
                      row.names = FALSE,
                      col.names = FALSE,
                      append = TRUE)
               log_msg("Chunk attached", flog.info)
          })
     }
}


sql_segmentation_n_persist <- function(x, dest_path){
        tryCatch({
                conn <- dbConnect(RSQLite::SQLite(), 
                                  file.path(dest_path, "database.db"))
                dbClearResult(dbSendQuery(conn, "PRAGMA busy_timeout=5000"))
                log_msg(paste("Tienda: ", x), flog.info)
                df <- dbGetQuery(conn, "select * from dataset_original
                                 where tienda = ?", params = x)
                log_msg(paste("Chunk Extract Length: ", nrow(df)), flog.info)
                name <- paste(dest_path, "preprocessed", x, sep = "/")
                file_name <- paste(name, ".csv", sep = "")
                log_msg(file_name, flog.info)
                fwrite(df, 
                       file_name,
                       row.names = FALSE,
                       col.names = FALSE,
                       append = TRUE)
                log_msg("Chunk attached", flog.info)
        })
}



#'@description Perform a data segmentation from a data.frame object or
#'ffdf connection.
#'@param x data.frame object or ffdf connection.
#'@param split_vector Vector of unique values in which the data
#'is going to be segmented.
#'@param dest_path Destination Directory Path
segmentation_n_persist <- function(x, split_vector, dest_path){
     for(i in 1:nrow(split_vector)){
          tienda <- split_vector[i, 1]
          q <- split_vector[i, 2]
          df <- x[x$tienda == tienda &
                       x$q == q, ]
          name <- paste(q, tienda, sep = "_")
          name <- paste(dest_path, "preprocessed",name, sep = "/")
          write.csv.ffdf(df, paste(name, ".csv", sep = ""),
                         append = TRUE)
     }
}


#'@description Retrieves a vector of unique values to
#'split another dataset.
#'@param X ffdf connection
#'@return Vector of uniques values.
get_split_vector <- function(X){
     unique_all <- subset.ffdf(X,
                               select = c(tienda,
                                          q))
     
     unique_all <- unique(unique_all)
     unique_all <- as.data.frame.ffdf(unique_all)
     return(unique_all)
}