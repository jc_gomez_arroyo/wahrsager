#' @author Wahrsager Team
#' 
#' Executors Script:
#' 
#' This script handles different executors for the Wahrsager execution
#' to provide different type of results depending on different
#' necessities. As a executor for only one forecast level, with a complete
#' proportions hierarchy; a executor with two different levels, etc.

#'@description One Level Wahrsager Executor
#'
#'This executor performs all Wahrsager procedures
#'but only with a one level forecast (selectionable)
one_level_executor <- function(level){
     
}