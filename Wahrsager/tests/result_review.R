setwd("C:/Users/yahirga/Documents/wahrsager/Wahrsager")
source("utilities.R")
source("data_cleaning.R")
source("feature_engineering.R")
source("data_segmentation.R")
source("data preprocess.R")
source("model_selection.R")
source("model_training.R")
source("model_evaluation.R")
source("forecast_splashing.R")
source("forecast_process.R")
library(gridExtra)


hfiles <- list.files(file.path(getwd(), "datasets", "preprocessed"),
                     full.names = TRUE)

historicaldt <- as.data.table(map_df(hfiles, fread))

names(historicaldt) <- expanded_vnames


ffiles <- list.files(file.path(getwd(), "datasets", "splash"))
ffiles <- ffiles[!is.na(str_match(ffiles, "^\\d+.csv"))]


forecasteddt <- map_df(file.path(getwd(), "datasets", "splash", ffiles), fread)
forecasteddt[, ':='(fecha = as.Date(fecha))]


unique(historicaldt[, ':='(fecha = as.Date(fecha),
                           Week = epiweek(fecha),
                           Year = year(fecha))][, 
                                                ':='(n = sum(n)), 
                                                by = .(Week, Year)][, 
                                                                    .(n, Week, Year)]) -> historicaldt
historicaldt[,
             ':='(fecha = 
                       ISOweek2date(paste(Year, 
                                          ifelse(nchar(Week) == 1, 
                                                 paste0("W0", Week), 
                                                 paste0("W", Week)), 
                                          1,
                                          sep = "-")))][, .(n, fecha)] -> historicaldt


forecasteddt <- unique(forecasteddt[,
                                   ':='(Week = epiweek(fecha),
                                   Year = year(fecha))][, 
                                    ':='(n = sum(n)), 
                                    by = .(Year, Week)][,
                                                   .(n, fecha)])

plot1 <- ggplot(historicaldt, aes(x = fecha, y = n)) +
     geom_line(color = "blue") +
     geom_line(data = forecasteddt, aes(x = fecha, y = n), 
               color = "red")

plot2 <- ggplot(forecasteddt, aes(x = fecha, y = n)) +
     geom_line(color = "blue")

grid.arrange(plot1, plot2)


     
tiendaf <- 11 
storef <- 235

forecasteddt <- fread(file.path(getwd(), "datasets", "output", "final_forecast.csv"))
names(forecasteddt) <- forecast_names

dt <- unique(forecasteddt[tienda == tiendaf & linea == storef , 
                   ][, fecha := as.Date(fecha)][,
                     ':='(n = sum(n)), 
                     by = .(fecha)][,
                                    .(n, fecha)])

ggplot(dt, aes(x = fecha, y = n)) + 
     geom_line(color = "red")


