source("utilities.R")
source("data_cleaning.R")
source("feature_engineering.R")
source("data_segmentation.R")
source("data preprocess.R")
source("model_selection.R")
source("model_training.R")
source("model_evaluation.R")
source("forecast_splashing.R")
source("forecast_process.R")
source("visual_utilities.R")

verify_pkgs()
set_properties()
dest_path <<- file.path(getwd(), "datasets")
library(shiny)
library(shinyFiles)
library(fs)

shinyServer(function(input, output, session) {
     
     volumes <- c(Home = fs::path_home(), "R Installation" = R.home(), 
                  getVolumes()())
     
     output_path <- file.path(dest_path, "output")
     output_files <- list.files(output_path)
     
     shinyFileChoose(input, "file", roots = volumes,
                     filetypes = c('csv'), session = session)
     shinyFileChoose(input, "qfile", roots = volumes,
                     filetypes = c('csv'), session = session)
     
     
     conn <<- dbConnect(RSQLite::SQLite(), file.path(dest_path, "database.db"))
     
     observeEvent(input$start_btn, {
          req(input$to_date)
          req(input$cores)
          req(input$file)
          req(input$qfile)
          
          tryCatch({
               log_msg("UI Execution", flog.info)
               input_path <- parseFilePaths(volumes, 
                                            input$file)$datapath
               quadrant_path <- parseFilePaths(volumes, 
                                               input$qfile)$datapath
               cores <- input$cores
               pkg <- input$pkg
               chunk_size <- input$chunk_size*10e6
               message(chunk_size)
               backend <- input$backend
               to_date <- input$to_date
               step_wise <- input$step_wise
               week_discontinued <- input$wdiscontinued
               week_new <- input$wnew
               CV <- input$cross_validation
               metrics <- input$metrics
               mape_threshold <- input$mape_threshold
               
               progress <- Progress$new(session, min = 1, max = 5)
               on.exit(progress$close())
               progress$set(message = "Running Forecasting")
               progress$set(value = 1)
               set_logger()
               
               preprocessing(data_path = input_path,
                             dest_path = dest_path,
                             chunk_size = chunk_size,
                             backend = backend)
               
               progress$set(value = 2)
               
               control.list <- par_forecast_process(dest_path = dest_path,
                                                    to.date = to_date,
                                                    step_wise = step_wise,
                                                    cores = cores, 
                                                    week_discontinued = week_discontinued,
                                                    week_new = week_new,
                                                    CV = CV,
                                                    pkg = pkg)
               
               progress$set(value = 3)
               
               best.models <- par_splash_process(dest_path = dest_path,
                                                 metrics = metrics,
                                                 cores = cores,
                                                 mape_threshold = mape_threshold)
               
               progress$set(value = 4)
               
               sankey_plot <- process_visualizations(best.models, dest_path)
               format2zauber(dest_path, quadrant_path, chunk_size)
               progress$set(value = 5)
          },
          error = function(e){
               log_msg(e$message, flog.error)
          }, finally = {
               closeAllConnections()
               log_msg("Closing all connections", flog.info)
          })
     })
     
     
     ready4viz <- reactive(!is_empty(output_files))
     
     
     observeEvent(ready4viz(), {
          progress <- Progress$new(session, min = 1, max = 5)
          on.exit(progress$close())
          progress$set(message = "Setting things up")
          progress$set(value = 1)
          
          best.model.dt <- dbReadTable(conn, "final_metadata") %>%
               data.table()
          
          progress$set(value = 2)
          lines <- reactive({
               req(input$tiendaso)
               lines <- best.model.dt[tienda == input$tiendaso, unique(linea)]
               return(lines)
          })
          
          models <- reactive({
               req(input$tiendaso)
               req(input$lineso)
               models <- dbGetQuery(conn, "select distinct model from models
                                   where tienda = ?
                                   and linea = ?", 
                                    params = list(input$tiendaso,
                                                  input$lineso)) %>%
                    data.table() %>% .[, unique(model)]
               return(models)
          })
          
          
          height.grid <- reactiveVal(1000)
          
          output$tienda_grid <- renderUI({
               selectInput('tienda_grid_input',
                           label = h5("Select a Store"),
                           choices = best.model.dt[tienda != "GENERAL", 
                                                   unique(tienda)])
          })
          
          output$linea_grid <- renderUI({
               req(input$tienda_grid_input)
               print(input$tienda_grid_input)
               selectInput('linea_grid_input',
                           label = h5("Select a Line"),
                           choices = best.model.dt[linea != "GENERAL" & 
                                                        tienda == input$tienda_grid_input, 
                                                   unique(linea)])
          })
          
          
          
          
          observeEvent(input$plot_grid_btn, {
               output$sku_grid <- renderPlot({
                    req(input$tienda_grid_input)
                    req(input$linea_grid_input)
                    height <- as.data.table(dbGetQuery(conn, "select count(distinct sku)
                                from dataset_preproc
                                where tienda = ?
                                and linea = ?", 
                                                       params = list(input$tienda_grid_input, input$linea_grid_input))) %>%
                         unlist() * 120
                    height.grid(height)
                    prepare_grid_sku(conn, input$tienda_grid_input,
                                     input$linea_grid_input)
               }, execOnResize = TRUE)
               
               output$plot_grid <- renderUI({
                    wellPanel(
                         plotOutput("sku_grid",
                                    width = "auto",
                                    height = height.grid()))
               })
               
          })
          
          output$tiendaso <- renderUI({
               selectInput('tiendaso',
                           label = h5("Select Store"),
                           choices = best.model.dt[, unique(tienda)])
          })
          
          output$lineso <- renderUI({
               selectInput('lineso',
                           label = h5("Select Line"),
                           choices = lines())
          })
          
          output$modelso <- renderUI({
               selectInput('modelso',
                           label = h5("Select Model"),
                           choices = models())
          })
          
          output$best_model <- renderText({
               req(input$tiendaso)
               req(input$lineso)
               
               best.model <- best.model.dt[tienda == input$tiendaso & linea == input$lineso, model]
               paste0("Best model: ", best.model)
          })
          
          output$tsplot <- renderPlot({
               req(input$tiendaso)
               req(input$lineso)
               req(input$modelso)
               progress <- Progress$new(session, min = 1, max = 3)
               on.exit(progress$close())
               progress$set(message = "Working...")
               progress$set(value = 1)
               
               dt <- prepare_model_dts(input$tiendaso, input$lineso, input$modelso)
               
               progress$set(value = 2)
               output$tsplot_info <- renderPrint({
                    nearPoints(dt, input$tsplot_hover, threshold = 35, maxpoints = 10)
               })
               p <- plot_model_result(dt)
               progress$set(value = 3)
               return(p)
          })
          
          progress$set(value = 3)
          
          output$accuracies <- renderDataTable({
               req(input$tiendaso)
               req(input$lineso)
               req(input$modelso)
               dbGetQuery(conn, "select * from accuracies
                                   where tienda = ?
                                   and linea = ?
                                   and model = ?",
                          params = list(input$tiendaso,
                                        input$lineso,
                                        input$modelso)) %>%
                    data.table()
          }, options = list(rownames = FALSE))
          
          output$model.metadata <- renderDT({
               req(input$tiendaso)
               req(input$lineso)
               req(input$modelso)
               best.model.dt[linea == input$lineso & tienda == input$tiendaso]
          }, options = list(rownames = FALSE))
          
          progress$set(value = 4)
          
          output$tsplot_agg <- renderPlot({
               progress <- Progress$new(session, min = 1, max = 3)
               on.exit(progress$close())
               progress$set(message = "Working...")
               progress$set(value = 1)
               
               agg.list <- prepare_agg_dts(conn)
               progress$set(value = 2)
               p <- plot_total_result(agg.list$forecast)
               output$tsplot_agg_info <- renderPrint({
                    nearPoints(agg.list$forecast, input$tsplot_agg_hover, threshold = 25, maxpoints = 10)
               })
               
               mape_reports <- create_aggregated_report(conn)
               
               output$mape_report_general <- mape_reports$mape_table
               output$mape_means <-  mape_reports$mape_means
               
               ts_data_f <- dt2ts(agg.list$forecast[f == "cleaned" | f == "forecast"])
               ts_data_o <- dt2ts(agg.list$forecast[f == "cleaned"])
               output$season_plot <- renderPlot({
                    plot_season_plot(ts_data_f)
               })
               output$decomposed <- renderPlot({
                    plot_decomposed(ts_data_o)
               })
               
               progress$set(value = 3)
               return(p)
          })
          
          output$sankey <- renderSankeyNetwork({
               nodes <- fread(file.path(dest_path, "output", "nodes.csv"))
               links <- fread(file.path(dest_path, "output", "links.csv"))
               sankeyNetwork(Links = links, Nodes = nodes,
                             Source = "source", Target = "target",
                             Value = "value", NodeID = "name",
                             fontSize = 18, units = "ts combinations")
               
          })
          
          metadata_plots <- plot_metadata(best.model.dt, dest_path)
          
          output$adf_hist <- renderPlot(metadata_plots$stationary.hist.adf)
          output$adf_dens <- renderPlot(metadata_plots$stationary.density.adf)
          output$kpss_hist <- renderPlot(metadata_plots$stationary.hist.kpss)
          output$kpss_dens <- renderPlot(metadata_plots$stationary.density.kpss)
          output$acf_hist <- renderPlot(metadata_plots$white.noise.hist)
          output$acf_dens <- renderPlot(metadata_plots$acf.density)
          output$metadata_table <- renderDT(best.model.dt,
                                            options = list(rownames = FALSE,
                                                           dom = 'Bfrtip',
                                                           autoWidth = TRUE,
                                                           buttons = c('csv')),
                                            extensions = c("Buttons", "FixedColumns"))
          output$special_cases <- renderDT({
               dbReadTable(conn, "error_cases") %>% data.table()
          }, options = list(rownames = FALSE,
                            dom = 'Bfrtip',
                            autoWidth = TRUE,
                            buttons = c('csv')),
          extensions = c("Buttons", "FixedColumns"))
          output$props <- renderDT({
               dbReadTable(conn, "proportions") %>% data.table()
          }, options = list(rownames = FALSE,
                            dom = 'Bfrtip',
                            autoWidth = TRUE,
                            buttons = c('csv')),
          extensions = c("Buttons", "FixedColumns"))
     })
     
})
