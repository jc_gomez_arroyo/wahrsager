#'@author SquirrelBirds Team
#'@description Dataset Preprocess Procedure
#'
#' This is a complete preprocess procedure for handling dataset preprocess.
#' The aim of this procedure is to execute sequentially dataset level
#' operations without persisting until all the operations are done.


#'@description Execute all the preprocessing steps. This is the
#'main function to call to perform all preprocessing at dataset
#'level operations.
#'@param data_path Path to dataset file
#'@param dest_path Path to destination directory
#'@param chunk_size Size of every chunk in bytes
#'@param backend Package Backend for all the preprocessing functions.
preprocessing <- function(data_path, dest_path, chunk_size, backend = "iotools"){
  tryCatch({
    log_msg("Preprocessing Procedure", flog.info)
    prepare.embedded.db(dest_path)
    
    log_msg("Removing interfiles", flog.info)
    check_folders(dest_path)
    remove_rfiles(dest_path)
    log_msg("Interfiles removed", flog.info)
    
    log_msg("Data Cleaning Process starting", flog.info)
    chunk_i <<- 1
    switch(backend,
           ff = {
      log_msg("FF Backend Running...", flog.info)
      ff_process(data_path = data_path,
                 dest_path = dest_path,
                 df_names = initial_vnames,
                 chunk_size = chunk_size)
      log_msg("Backing Special Cases into Database", flog.info)
      clean.special.cases(dest_path)
      log_msg("Special Cases Table created", flog.info)
    },
    iotools = {
      log_msg("iotools Backend Running...", flog.info)
      iotools_process(data_path = data_path,
                      dest_path = dest_path,
                      chunk_size = chunk_size)
      log_msg("Backing Special Cases into Database", flog.info)
      clean.special.cases(dest_path)
      log_msg("Special Cases Table created", flog.info)
    },
    sqlite = {
      log_msg("SQLite Backend Running...", flog.info)
      sqlite_preprocess(data_path = data_path,
                        dest_path = dest_path)
    })
    
  },
  finally = {
    log_msg("Finally: Closing Connections", flog.info)
    dbDisconnect(conn)
    closeAllConnections()
  })
}


#'@description Preprocessing function for FF backend.
#'@param data_path Path to dataset file.
#'@param chunk_size Size of chunk in bytes.
#'@param df_names Character Vector for names in dataset.
#'@param dest_path Destination Directory Path
ff_process <- function(data_path, chunk_size, df_names, dest_path) {
  set_ff_options(dest_path)
  df_ff <- read.table.ffdf(file = data_path,
                           sep = ",", 
                           VERBOSE = TRUE, 
                           header = FALSE,
                           next.rows = chunk_size)
  log_msg("Dataset loaded with FF Backend", flog.info)
  names(df_ff) <- df_names
  log_msg("Names defined", flog.info)
  split_vector <- get_split_vector(df_ff)
  log_msg("Vector of Unique values for segmentation retrieved", flog.info)
  cleaning_n_separate(df_ff, split_vector, dest_path)
}

#'@description Preprocessing function for iotools backend.
#'@param data_path Path to dataset file.
#'@param chunk_size Size of chunk in bytes.
#'@param dest_path Destination Directory Path
iotools_process <- function(data_path, dest_path, chunk_size) {
  chunk_i <<- 1
  chunk.apply(data_path,
              FUN = function(chunk){
                dt <- make_dt(chunk, 5)
                dt_cleaning_n_separate(dt, dest_path)
              },
              CH.MAX.SIZE = chunk_size
  )
  log_msg("Dataset Cleaned and Separated by Tienda", flog.info)
}

sqlite_preprocess <- function(data_path, dest_path){
  log_msg("Loading dataset...", flog.info)
  tables <- c("dataset_preproc", "dataset_props")
  for(table in tables)(try(dbRemoveTable(conn, table),
      silent = TRUE))
  dbWriteTable(conn, "dataset_preproc", data_path,
               colClasses = c(rep("character", 3),
                              "Date", "numeric"))

  dbWriteTable(conn, "dataset_props", proportions_path,
               colClasses = c(rep("character", 3),
                              "Date", "numeric"))
  
  log_msg("Original Dataset loaded. Cleaning...")
  dbExecute(conn, "update dataset_preproc set n = 0
            where n < 0")
  
  dbExecute(conn, "update dataset_props set fecha = date(fecha)")
  dbExecute(conn, "alter table dataset_props add Month INT(3)") 
  dbExecute(conn, "alter table dataset_props add Year INT(4)")
  dbExecute(conn, "update dataset_props set Month = strftime('%m', fecha),
                    Year = strftime('%Y', fecha)")
  
  dbExecute(conn, "update dataset_preproc set fecha  = date(fecha)")
  dbExecute(conn, "alter table dataset_preproc add Month INT(3)") 
  dbExecute(conn, "alter table dataset_preproc add Year INT(4)")
  dbExecute(conn, "update dataset_preproc set Month = strftime('%m', fecha),
                    Year = strftime('%Y', fecha)")
  
  clean.special.cases(dest_path)
  log_msg("Dataset loaded and cleaned", flog.info)
}


#'@description Perform a data.table manipulation for all
#'preprocess functions.
#'@param chunk_dt Data.table object
#'@param dest_path Destination Directory Path
dt_cleaning_n_separate <- function(chunk_dt, dest_path) {
  tryCatch({
    log_msg(paste("Chunk: ", chunk_i), flog.info)
    dt <- dt_struct_conv_chunk(chunk_dt)
    dt <- dt_replace_na_chunk(dt)
    dt <- dt_imputation_chunk(dt)
    log_msg("Imputation and NAs Replacement, done", flog.info)
    
    dt <- dt_feature_generate(dt)
    log_msg("Feature Generation, done", flog.info)
    save2DB(conn, dt, "dataset_preproc")
    dt <- dt_segmentation_n_persist(dt, dest_path)
    log_msg("Chunk attached to Database by Tienda", flog.info)
    chunk_i <<- chunk_i + 1
  })
}


#'@description Perform all preprocess functions for
#'FF Backend.
#'@param x Data.frame object
#'@param split_vector Vector of unique values to segment data.
#'@param dest_path Destination Directory Path
cleaning_n_separate <- function(df, split_vector, dest_path) {
  tryCatch({
    df <- replace_na(df)
    df <- data_imputation(df)
    log_msg("Imputation and NAs Replacement, done", flog.info)
    
    df <- ff_feature_generate(df)
    log_msg("Feature Generation, done", flog.info)
    
    df <- segmentation_n_persist(df, split_vector, dest_path)
    log_msg("Chunk attached by Tienda", flog.info)
  })
}

clean.special.cases <- function(dest_path){
  new.combs <- lookup.new.levels() %>% .[, case := "new"]
  disc.combs <- lookup.disc.levels() %>% .[, case := "discontinued"]
  error.cases <- rbind(new.combs, disc.combs)
  tables <- c("error_cases", "dataset_clean")
  for(table in tables){
    try(dbRemoveTable(conn, table),
      silent = TRUE)
    }
  save2DB(conn, error.cases, "error_cases")
  fwrite(error.cases, file.path(dest_path, "model", "error_cases.csv"),
         row.names = FALSE, col.names = FALSE, append = FALSE)
  dbWriteTable(conn, "dataset_clean", 
  dbGetQuery(conn, "select t1.sku, t1.linea, t1.tienda, 
            t1.fecha, t1.n, t1.Month, t1.Year
           from dataset_preproc t1 
           left join error_cases t2 
           on t1.sku = t2.sku
           and t1.tienda = t2.tienda
           and t1.linea = t2.linea 
           where `case` IS NULL"))
  dbWriteTable(conn, "dataset_props_clean", 
               dbGetQuery(conn, "select t1.sku, t1.linea, t1.tienda, 
                          t1.fecha, t1.n, t1.Month, t1.Year
                          from dataset_props t1 
                          left join error_cases t2 
                          on t1.sku = t2.sku
                          and t1.tienda = t2.tienda
                          and t1.linea = t2.linea 
                          where `case` IS NULL"))
  log_msg("Dataset cleaned", flog.info)
}


lookup.new.levels <- function(){
  
  new.threshold <- as.character(dbGetQuery(conn, 
                              "select max(fecha) as max_date 
                              from dataset_preproc") %>% flatten_chr() %>%
    as.Date() - (week_new * 7))
  
  new.levels <- as.data.table(dbGetQuery(conn,
                                         "select t1.tienda, t1.linea, t1.sku, t2.first_date 
               from dataset_preproc t1
               inner join (select * 
               from (
               select sku, min(fecha) as first_date
               from dataset_preproc 
               group by sku) sub  
               where sub.first_date >= ?) t2
               on t1.sku = t2.sku", 
                                         params = new.threshold)) %>%
    .[, first_date := NULL]
  
  return(new.levels)
}

lookup.disc.levels <- function(){
  
  disc.threshold <- as.character(dbGetQuery(conn, 
                                          "select max(fecha) 
                                         from dataset_preproc") %>%
                  flatten_chr() %>% as.Date() - (week_discontinued * 7))
  
  disc.levels <- as.data.table(dbGetQuery(conn,
                                          "select t1.tienda, t1.linea, t1.sku, t2.last_date 
               from dataset_preproc t1
               inner join (select * 
               from (
               select sku, linea, tienda, max(fecha) as last_date
               from dataset_preproc 
               group by sku, linea, tienda) sub  
               where sub.last_date <= ?) t2
               on t1.sku = t2.sku
               and t1.linea = t2.linea
               and t1.tienda = t2.tienda", 
                                          params = disc.threshold)) %>%
    .[, last_date := NULL]
  
  return(disc.levels)
}
