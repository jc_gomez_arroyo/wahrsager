# Code Metodology

Here we can find the metrics and code conventions to follow during the project development.

## Name Convention

### Variable Names
The convention for naming variables will be the same inside and outside sub-functions.
1. No camel case. This means variables with first letter downcase and the next word with uppercase: dataset_Path, datasetPath, dataPath.
2. Use underscore for separate words: dataset_path, pkgs_names, model_trainer.
3. Maximum 6 words for variable names, as it has to be a descriptive name but not so large: dataset_clean_path_descriptor.
4. Take advantage of abreviations for some names: packages - pkgs, clean - cln, linear_regresion_model - lrm.

### Function Names
Convention for naming functions is similar to variable names but require a few more considerations.
1. No camel case.
2. Use underscore for separate words.
3. Maximum 8 words for function names, as it has to be a descriptive name but not so large. It needs a concise name, that describe the main idea of what the function is doing and what we can expect as the return value.
4. Maximum 4 words for parameter names.
5. Take advantage of abreviations for some names.
6. One parameter at least as not nulleable. This means that when we require a function with a parameter, at least the first one should not have a default value.
	For example: 
	
	>	power_of_2(n){
	>		n * n
	>	} 
	
	This function would need a value to compute n^2


## Metrics

This metrics are for optimal method design and problem abstraction. The best idea is to fit to this metrics as a guideline, but sometimes a function could be larger if there is no option. We have to 'try' to adjust to the metrics.

### Function Length
* No more than 30 lines

### Line Width
* No more than 85 characters.



## Programming conventions

### Concepts

We will try to code with the three main concepts of OOP (Object Oriented Programming), or at least some of them as R works more as a functional programming language and scientific programming language than a software development language. The concepts are:

1. High Cohesion: We have to define every function and class (in R would be for now the R file) with a concise functionality in the system. This means that we design a group of functions that work for a specific problem in the system. If we need some data collection class, we could have many types of data collectors from Databases, from API's, from Text Files, from Streaming Data. All of those would be in one class, or R file to improve the reusability of the code and the maintenance.

2. Low Coupling: This might be optional as it means the degree of dependency that a class has with others. For a class to be designed with low coupling, we would design the class independent of the others classes. This means that our functions inside the class can work without retrieving variables from other classes as much as we can. Or we would need to point to some requisites of one class (its functionality) instead of many directions. For example, for the data transformation module we would expect to have a data extraction module to pass the data.

			data transformation module -> depends on -> data extraction module

	This way, we know where to focus when there's a problem in the code. Knowing that a module depends on a specific class, we know where to start or what to change.


3. High Abstraction: Probably the most important one, but here we would apply it in a reduced level. The abstraction in computer science means to visualize a problem as a object with some methods and some attributes and how to manage them to solve it. High Abstraction in a function means to model it so well that we can reuse it for new solutions. Here we won't use the abstraction method for the class but for the functions only.


There many concepts, but these are the most important for now.


### Thing to keep in mind

* Even if we have defined our modules for the system, this doesn't mean that we won't need more R files/packages/classes to model solutions. It can be useful to have some utilities classes that solve repetitive tasks that are not bounded to a module funtionality. 

* Our objective is to apply concurrent programming, so we have to understand really well some concepts as "Split-Apply-Combine" which is similar to the "MapReduce" concept.
