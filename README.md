
![User Interface Mockup](https://i.imgur.com/BoVxwbz.png)

# Warhsager
Web Application for Retail Demand Forecasting, which use a variety of models like ARIMA, Exponential Smoothing, Naive, Linear Regression (Univariate & Multivariate), Robust Estimators, XGBoost, etc. Implements a Machine Learning Algorithm for Automatic Model Selection.


## Requirements

### Packages:

*_bigmemory:_ Chunk-Data Processing and Hard Disk Data Processing.
*_iotools:_ Scalable Code and Parallel Processing
*_sparklyr:_ Hadoop Infrastructure implementation, Spark functionality libraries.
*_shiny:_ Web Application Framework for R.
*_logging:_ Python based logging module library.


### References:
Every metodology, package, or general knowledge for the development of this project.

#### Books & Papers

* Hans Levenbach, (2017), Change & Chance Embraced: Achieving Agility with Smarter Forecasting in the Supply Chain, Delphus Publishing.
* Rob J Hyndman, et al., (2007), Optimal combination forecasts for hierarchical time series, Department of Econometrics and Business Statistics, Monash University.
* Rob J Hyndman, et al., (2018), Forecasting: Principles and Practice, OTexts
* Robert C. Martin, (2008), Clean Code: A Handbook of Agile Software Craftsmanship, Prentice Hall.
* Raffaele Iannone, et al., (2013), Merchandise and Replenishment Planning Optimisation for Fashion Retail, University of Salerno.
* Martin Klepmann, (2016), Designing Data-Intensive Applications: The Big Ideas Behind Reliable, Scalable, and Maintainable Systems, O'Reilly Media.
* Roger D. Peng, et al., (2017), Mastering Software Development in R, LeanPub.
* Brian Caffo, et al., (2018), Executive Data Science, LeanPub

