#Technology Documentation

Here are some references if you need them for the project development.


## Git

* Git Official documentation: https://git-scm.com/doc
* "Introduction to Git for Data Science" DataCamp course: https://www.datacamp.com/courses/introduction-to-git-for-data-science

## R

* R Official Documentation: https://www.rdocumentation.org/


## Shiny Web App Framework

* Shiny Official Tutorial: https://shiny.rstudio.com/tutorial/
* Shiny Functions Reference: https://shiny.rstudio.com/reference/shiny/1.2.0/


## bigmemory Package

* Tuturial PDF: http://www.chrisbilder.com/compstat/presentations/Xiaojuan/Presentation_bigmemory.pdf
* Journal PDF: http://www.stat.yale.edu/~mjk56/temp/bigmemory-vignette.pdf

